Silence, on tourne
===================

Montage de plusieurs sources :

* une vidéo issue d'un camescope (fichier rush_video.WMV) ;

* deux vidéo YouTube (fichier teetrix.mp4 et octobre.flv) ;

* des scenes 3D Blender des animations précédentes :

* des scènes 3D Blender de titrage ;

* une bande son ;

Découpe et placement des vidéoSS
---------------------------------

.. image:: media/sequence_window.*
   :scale: 75
   :align: center 

Se mettre en mode "Sequence"
Placer les différents strip sur le plan de montage

.. image:: media/strips.*
   :scale: 75
   :align: center 

Faire un premier découpage des rush vidéos (avec la touche K)

Strip scene : alpha over

.. image:: media/scene_alpha_over.*
   :scale: 75
   :align: center 

Scene : render, premu

.. image:: media/scene_render_premu.*
   :scale: 75
   :align: center 

Une vidéo dans la vidéo
------------------------

Modifier le screenshot des écrans en films
Créer une nouvelle texture
Option movie : cyclic, Auto refresh, et btn '<'

fichier : jm2l2009_PhE_step_G.blend

Ajout d'un bandeau en surimpression
-------------------------------------

On va placer un logo et un titre 'JMLL 2009' en bas 

via inkscape

.. image:: media/inkscape.*
   :scale: 75
   :align: center 

Ajouter l'image bas_de_page.png + premul + alpha over
Elargir pour que le strip occupe tout le film



Ajout d'un titre en 3D
------------------------

.. image:: media/titre.*
   :scale: 75
   :align: center 

Ajouter un titre dans une nouvelle scene
ajouter du texte, centrer et mettre une valeur à "extrude"
Créer un "material" avec une couleur vive

.. image:: media/titre_props.*
   :scale: 75
   :align: center 

Faire une animation de 400 images avec un déplacement de la caméra
Pour le rendu par F12 pensez à décocher "sequence" dans les propriétés "scene"
Ajouter la scene à la sequence + alpha over
Superposer légèrement le "strip" du titre avec celui de la première vidéo
Ajouter une transition "cross"
Bien placer le cross au dessus des 2 pistes

fichier : jm2l2009_PhE_step_H.blend

### AJouter des transitions

### Utiliser plusieurs séquences déjà rendues
### Faire quelques plans avec des transitions
