Un peu de couleur
==================

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_D.blend*.

Nous allons appliquer des matières et textures à 
nos objets afin qu'il paraissent plus réalistes.

A tout moment, vous pouvez demander à Blender de calculer
un rendu de votre scène (Touche **F12**) :

.. image:: media/rendu_model.*
   :scale: 75
   :align: center 

D'abord nous allons dupliquer l'ensemble des 2 écrans
avec leur bras pour former une *marguerite* de 4 écrans.
Cette disposition nous permettra d'apprécier l'effet de nos
matières et textures.

Sélectionnez la fixation double sur laquelle reposent les bras.
Ensuite, étendez la sélection à toute la descendance d'objets par
*Select/Grouped/Children* (Touche **shift-g, 1**).
Vous pouvez alors dupliquer l'ensemble en conservant les liens par
*Duplicate Linked* (Touche **alt-d**).
Sans perdre la sélection courante, vous pouvez amorcer une rotation
de l'ensemble (Touche **r**).
Nous obtenons la disposition suivante :

.. image:: media/marguerite.*
   :scale: 75
   :align: center 


A ce stade, si nous demandons un rendu de la scène, nous obtenons
une image assez fade :

.. image:: media/fade.*
   :scale: 75
   :align: center 

Inox et plastique
-------------------

Nous allons créer une première matière que l'on nommera *MA:inox*.
Sélectionnons d'abord un des bras et affichons ses propriétés
de matière. C'est ici que nous pouvons choisir de créer une nouvelle matière et lui donnée le nom voulu :

.. image:: media/ma_inox.*
   :scale: 75
   :align: center 

Pour obtenir l'aspect "inox" recherché, nous appliquerons un effet
de mirroir avec la propriété RayMir à environ 0,5.

Pour les écrans, nous allons procéder de la même façon pour créer
notre matière *MA:gris*. A la différence que nous n'activerons pas
l'effet mirroir et nous contenterons de fixer une couleur grise :

Pour les écrans, nous ne pouvons appliquer uniformément la matière
*MA:gris* car ceux ci comportent une fixation que l'on souhaite voir
avec la matière *MA:inox* (sans compter sur l'image affiché par
l'écran qui aura sa propre matière).

Pour appliquer plusieurs matières sur un même objet, nous devons
le faire en mode *édition* et non en mode *objet* (Touche **tab**).
En mode édition, lorsque plusieur faces sont sélectionnées, nous
affichons leurs propriétés :

.. image:: media/prop_face.*
   :scale: 75
   :align: center 

Il est alors possible d'appliquer une matière sur les faces
sélectionnées uniquement.

Image des écrans
------------------

Afin que nos écrans affichent quelque chose, nous allons créer une
matière dont la particularité sera d'afficher une image bitmap au
format JPEG ou PNG.

Commençons par sélectionner la face de l'écran qui contiendra notre nouvelle
matière : sélection de l'objet (Bouton **droit**), passage en mode édition
(Touche **tab**), déselection de la sélection précédente (une ou deux fois la
touche **aa**), sélection par face (Touche **ctrl-tab, 3**) et sélection
de la face concernée (Bouton **droit**).

Affichons les propriétés de la sélection et demandons la création d'une
nouvelle matière appelée *MA:screenshoot* :

### image

Nous pouvons alors afficher les propriétés de notre nouvelle matière.
Dans le panneau consacré aux textures, nous créons une nouvelle texture
appelée *TE:screenshot* :

.. image:: media/ma_ss.*
   :scale: 75
   :align: center 

Maintenant nous allons modifier la texture nouvellement créée. Fixons le type
de la texture à *Image*. Dans le panneau *Image* cliquons sur l'icône
de répertoire pour sélectionner le fichier image à charger.
Votre texture devrait ressembler à ceci :
   
.. image:: media/te_ss.*
   :scale: 75
   :align: center 

Revenons dans les propriétés de notre matière *MA:screenshot* pour cocher
l'option 'Cube' :
   
.. image:: media/ma_ss_mi.*
   :scale: 75
   :align: center 

### image
Plus lumineux : shader emit = 1.0

Voici le rendu que l'on obtient après application de nos 3 matières 
(Touche **F12**) :

.. image:: media/rendu_matiere.*
   :scale: 75
   :align: center 

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_E.blend*.

