Il faut que ça bouge
======================

Nous allons maintenant animer notre scène.

Blender dispose d'un mode *Animation* qui va nous permettre de faire prendre
des poses à nos objects au cours du temps. 
Le logiciel va alors interpoller les positions intermédiaires nous évitant
ainsi une fastidieuse animation image par image.

.. image:: media/animation.*
   :scale: 75
   :align: center 

Passons notre environnement de travail en *SR:1-Animation* et plaçons-nous
sur la première image de notre animation.

Images clées
-------------

Nous allons maintenant indiquer à Blender quels sont les objets que l'on veut
faire poser sur cette première image. Sélectionnons toute notre installation
(hormis la lampe et la caméra). Pour celà nous pouvons sélectionner la barre
verticale et demander à sélectionner tous ces enfants par
*Select/Grouped/Children* (Touche **shift-g, 1**).

Maintenant indiquons quels sont les paramêtres qui doivent être enregistrés en
insérent un clé *location/rotation* (Touche **i**) :

.. image:: media/loc_rot.*
   :scale: 75
   :align: center 

Nous souhaitons une animation avec 3 images clés aux position 1, 100 et 200.
Répétons l'opération consistant à ajouter une clé *location/rotation* aux
positions 100 et 200.

Les positions 1 et 200 seront les positions de repos. Nous ne déplaçons donc
pas nos objets sur ces positions. Par contre, nous pouvons modifier la posture
de nos objets sur l'image 100.
Essayons de prendre la pose suivante :

.. image:: media/pose.*
   :scale: 75
   :align: center 

Position interpolée
------------------------

Nous pouvons consulter les positions intermédiaires et constater que Blender
calcule automatiquement la position de nos objets.
L'ensemble de ces positions constituent des *courbes IPO* :

.. image:: media/ipos.*
   :scale: 75
   :align: center 

Il est parfois nécessaire d'ajuster manuellement ces courbes pour par exemple
corriger une abhération : Des écrans qui se chevauchent pendant un mouvement.
Les courbes sont modifiées en passant en mode *édition* (Touche **tab**).

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_F.blend*.


