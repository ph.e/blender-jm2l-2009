Modélisation
===============

On va commencer par une modélisation grossière avant d'affiner.
La modélisation va consister à créer des *boites* aux bonnes dimensions.
Voici le résultats que nous souhaitons obtenir :

.. image:: media/boites.*
   :scale: 75
   :align: center 

Voici les objets à créer :

* Création d'un écran : dimension 480x280x7000, nom OB:ecran, ME:ecran

* Création d'un bras : dimension 200x60x60, nom OB:bras, ME:bras

* Création d'une fixation double bras : 120x120x60, nom OB:fixation, ME:fixation

* Création d'une barre : dimension 40x40x700, nom OB:barre, ME:barre

Les étapes sont toujours les mêmes : On crée un cube (Touche **espace**) :

.. image:: media/cube.*
   :scale: 75
   :align: center 

On change sa taille. On pourrait le faire en mode *objet*, mais on préfère le 
faire ne mode édition (Touche **tab**) afin de conserver une échelle de *1*
pour tout nos objets. Vous saisirez cette subtilité avec un peu d'expérience.
Lorsque l'on est en mode *édition*, on sélectionne tous les sommets
(Touche **a**) puis on modifie l'échelle globale (Touche **s**) et enfin
on valide (Bouton **gauche**).


Centre de rotation
--------------------

.. image:: media/centre.*
   :scale: 75
   :align: center 

Chaque objet dispose d'un centre. Il est utilisé, entre autres, comme centre
de rotation. Nos objets étant articulés les uns aux autres (écran sur bras, 
bras sur bras, bras sur fixation, fixation sur barre), il est utile de placer
correctement ce centre dès le début de la modélisation.

Dans le schéma ci-dessus, le centre est matérialisé par un cercle rose de
quelques pixels. Pour déplacer le centre, passez en mode édition
(Touche **tab**), sélectionner l'ensemble du *mesh* (Touche **a**) et opérez
un déplacement (Touche **g**). Quand le centre est bien placé, validez
(Bouton **gauche**).

Tel père, tel fils
--------------------

Un objet peut être le descendant d'un autre objet. L'un des intérêt de cette
filiation est qu'une transformation appliqué au parent sera directement
répercutée sur les enfants. Ainsi une translation ou une rotation appliquée
à un parent sera également appliquée aux enfants.

Pour créer une relation parent-enfant, sélectionner l'enfant (Bouton **droit**),
puis le parent (Touche **shift** + Bouton **droit**) et créer le lien de parenté
(Touche **ctrl+p**) :

.. image:: media/make_parent.*
   :scale: 75
   :align: center 

Ces liens de parenté vous nous simplifier l'articulation des objets.
Notez qu'il existe un mécanisme beaucoup plus sophistiqué appelé *armature*
qui permets de gérer des relations beaucoup plus complexes.

Clone, clone, clone
---------------------

Nous allons maintenant cloner nos objets de références pour composer une scène
sommaire : une barre, une fixation double, quatre bras (deux à deux) et deux
écrans.

Pour celà, nous créons des copies liées de nos objets. Sélectionner l'objet
à cloner (Bouton **droit**) et dupliquer-le (Touche **alt+d**) :

.. image:: media/duplicate_linked.*
   :scale: 75
   :align: center 

Vous pouvez dupliquer les objets un par un et recréer les liens de parenté ou
dupliquer un ensemble d'objets (ceux-ci conservant alors leur lien de parenté).

Vous pouvez passer dans le mode *SR:1-Animation* pour visualiser la
hiérarchie complète de votre scène :

.. image:: media/hierarchie.*
   :scale: 75
   :align: center 

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_A.blend*.


Modélisation d'un écran plat
-----------------------------

Nous allons maintennat détailler la modélisation de l'écran plat.
Nous utiliserons la translation et l'extrusion pour arriver à
ce résultat à partir de notre *cube* :

.. image:: media/lcd.*
   :scale: 75
   :align: center 

Sélectionner l'objet (Bouton **droit**) et passez en mode édition
(Touche **tab**). Déselectionner tous les sommets (Touche **a**).
Sélectionner une face du cube (Bouton **droit** sur les 
sommets, ou Touche **b** pour sélection rectangulaire, ou Touche **bb** pour
sélection au *pinceau*). Déplacez la sélection pour *amincir* le cube
(Touche **g** ou Touche **gx** pour contraindre sur l'axe X
et Bouton **gauche** pour valider) :

.. image:: media/lcd_model1.*
   :scale: 75
   :align: center 

Avec notre face toujours sélectionnée, nous pouvons demander une extrusion
(Touche **e**, menu *region* et Bouton **gauche**) :

.. image:: media/extrude_region.*
   :scale: 75
   :align: center 

Nous pouvons changer la taille de cette face (Touche **s** et Bouton **gauche**)
ainsi que la position (Touche **g** et Bouton **gauche**) :

.. image:: media/lcd_model2.*
   :scale: 75
   :align: center 

Notez que les opérations de changement de taille et de déplacement peuvent
être affinées en ajoutant une contrainte sur l'axe :

* Touche **gx** : déplacement uniquement sur l'axe des x 

* Touche **sy** : changement de taille uniquement sur l'axe des y

* Touche **gy2** : déplacement de 2 sur l'axe y

* Touche **gy-2** : déplacement de -2 sur l'axe y

Vous pouvez affiner le modèle pour y faire apparaître plus de détails.


Modélisation d'un bras 
-----------------------

Nous allons maintenant détailler la modélisation du bras.
Nous utiliserons les *modifiers* ainsi que l'outils *loop subdivide*
pour arriver à ce résultat : 

.. image:: media/bras_model.*
   :scale: 75
   :align: center 

L'objet étant plus complexe mais néanmoins symétrique, nous utiliserons
cette propriété pour réduire notre effort de modélisation. En effet, nous ne
modéliserons qu'un seul côté de l'objet, laissant à Blender le soin d'opérer
la symétrie.
Pour celà nous utiliserons un *mirror modifier*. Sélectionner l'objet et
afficher ces propriétés :

.. image:: media/modifier.*
   :scale: 75
   :align: center 

Ajoutons le *mirror modifier* avec la propriété Y cochée : 

.. image:: media/mirror.*
   :scale: 75
   :align: center 

Désormais toutes les modifications apportées à l'objet en mode édition seront
affichée avec une symétrie sur l'axe des Y (Assurez-vous de bien avoir décaler
le centre de rotation sur l'axe Y sinon la symétrie ne sera pas visible).

Nous pouvons commencer à détailler notre objet. En mode édition
(Touche **tab**), nous pouvons ajouter des subdivisions (Touche **ctrl+r**).
Vous devez valider le plan dans lequel opérer la subdivision puis la position
de celle-ci. Après validation (Bouton **gauche**), vous pouvez changer la taille
(Touche **s**) ou la position (Touche **g**) :

.. image:: media/subdivide.*
   :scale: 75
   :align: center 

Pour pouvez donner un aspect beaucoup moins anguleux à notre objet sans avoir
à créer nous-même toutes les facettes nécessaires. Ajoutons un *subsurf
modifier* avec la propriété *Levels* à 2 :

.. image:: media/subsurf_prop.*
   :scale: 75
   :align: center 

Il est souvent utile de supprimer la face entre les 2 parties du mirroir pour éviter que le *subsurf* ne s'applique à cet endroit.

Nous avons ainsi les outils utiles à la modélisation de notre bras :

.. image:: media/subsurf_model.*
   :scale: 75
   :align: center 

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_B.blend*.

Lorsque la modélisation du bras est convenable, nous appliquons les
*modifiers* ce qui va générer les éléments de maillage correspondants.
Il suffit de cliquer sur le bouton *Apply* de chaque *modifier*.

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_C.blend*.

Modélisation de la fixation
----------------------------

Nous pouvons utiliser les outils évoqués précédemment pour modéliser la 
fixation double :

.. image:: media/fixation.*
   :scale: 75
   :align: center 

Les outils complémentaires utiles sont : la suppresion d'un sommet, arrête
ou face (Touche **x**), la création d'une arrête ou d'une face (Touche **f**
après sélection de 2 ou plusieurs sommets) et le rapprochement de deux
sommets (Touche **alt+m**).

.. image:: media/fixation_modifier.*
   :scale: 75
   :align: center 

A cette étape de l'atelier, vous pouvez utiliser le fichier *jm2l2009_PhE_step_D.blend*.
