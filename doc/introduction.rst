Introduction
==============

Cet atelier a pour but de vous initier à Blender, logiciel de modélisation
3D, séquenceur vidéo et moteur de jeu.

Cette initiation n'est qu'un survol des immenses possibilités de Blender.
Mais c'est surtout une tentative de démistification de cet outils souvent
loué pour sa puissance et décrié pour sa complexité.
Avec moins de cinquante heures de vol sous Blender, je suis encore largement
dans la catégorie des débutants. Et c'est le témoignage d'un débutant que
je souhaite apporter à des novices.
Il est certe nécessaire d'investir quelques heures dans l'outils avant d'en
tirer un résultat mais nul besoin d'avoir suivi une formation d'infographiste.
Nous réaliserons une petit film entièrement sous Blender de la modélisation
au montage final.

.. image:: media/intro.*
   :scale: 75
   :align: center 

Nous modéliserons des écrans plats fixés sur des bras articulés.
Nous animerons ces articulations et intégrerons l'ensemble dans un
montage vidéo composé de plusieurs sources et d'une bande son.

Ce document n'est ni un mode d'emploi, ni un turoriel.
Considérez le comme un support de l'atelier.

