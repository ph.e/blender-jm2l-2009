.. atelier_blender_jmll2009 documentation master file, created by
   sphinx-quickstart on Tue Nov 24 14:08:52 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Atelier Blender des JMLL 2009
===============================

.. toctree::
   :maxdepth: 2

   introduction
   modelisation
   texture
   animation
   montage_video
   credits

