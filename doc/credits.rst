Crédits
=========

License
--------

Ce document et les fichiers associés ont été réalisé par
Philippe ENTZMANN (philippe.entzmann@gmail.com)
sous licence Creative Commons CC-by-sa.
http://creativecommons.org/licenses/by-sa/2.0/fr/

Outils
-------

Cet atelier a été réalisé avec la version 2.49b de Blender.
Vous pouvez également jetter un oeil sur la toute nouvelle version 2.5
dont l'interface a été profondement remaniée.
http://www.blender.org

Nous avons également utilisé Inkscape.
http://www.inkscape.org

La présente documentation a été réalisée avec Sphinx.
http://sphinx.pocoo.org

Le tout a mijoté plusieurs heures sous Ubuntu Linux.
http://www.ubuntu-fr.org

Crédits audio et vidéo
----------------------

Vidéo "Teeworlds Z-Wolf" par staffGMs1. http://www.youtube.com/watch?v=XsxdGzalg38

Vidéo "Octobre" par Pavlvs. http://www.youtube.com/watch?v=F9nPpEOmnnI

Chanson "Total Breakdown" de Brad Sucks. http://www.jamendo.com/fr/album/31187

Chanson "Defrag" de Binärpilot. http://www.jamendo.com/fr/album/1145#

